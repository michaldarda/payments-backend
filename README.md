# Payments

To start your new Phoenix application:

1. Install dependencies with `mix deps.get`
2. Start Phoenix endpoint with `mix phoenix.server`

Now you can visit `localhost:4000` from your browser.

# Save tokens of all users to csv file

    docker run -it --link payments_db:postgres --rm postgres sh -c 'PGPASSWORD=postgres PAGER=cat exec psql -h "$POSTGRES_PORT_5432_TCP_ADDR" -p "$POSTGRES_PORT_5432_TCP_PORT" -U postgres -d payments_prod -t -A -F"," -c "select token from users;"' > tokens.csv 
