use Mix.Config

# In this file, we keep production configuration that
# you likely want to automate and keep it away from
# your version control system.
config :payments, Payments.Endpoint,
  secret_key_base: "9tfJ6r/tUVdkoJd5uPQX+U9M/AJjoUHeIfIDy67T4qY11AUygCp8acnnk++HB3ME"

# Configure your database
config :payments, Payments.Repo,
  adapter: Ecto.Adapters.Postgres,
  url: "ecto://postgres:postgres@payments_db/payments_prod",
  size: 20 # The amount of database connections in the pool

config :exq,
  host: 'redis',
  port: 6379,
  namespace: "exq",
  concurrency: :infinite,
  queues: ["default"],
  poll_timeout: 50,
  scheduler_enable: false,
  scheduler_poll_timeout: 200
