use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :payments, Payments.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :payments, Payments.Repo,
  adapter: Ecto.Adapters.Postgres,
  pool: Ecto.Adapters.SQL.Sandbox,
  username: "ecto",
  password: "ecto",
  database: "payments_test",
  size: 1 # Use a single connection for transactional tests

config :exq,
  host: '127.0.0.1',
  port: 6379,
  namespace: "exq",
  concurrency: :infinite,
  queues: ["default"],
  poll_timeout: 50,
  scheduler_enable: false,
  scheduler_poll_timeout: 200
