defmodule Payments.ExampleData do
  alias Payments.Account
  alias Payments.User
  alias Payments.Repo

  import Comeonin.Bcrypt, only: [hashpwsalt: 1]

  def create_users_and_accounts(amount) do
    Enum.each (0..amount), fn(_i) ->
      token =
      Comeonin.Tools.random_bytes(25)
      |> :binary.bin_to_list
      |> Comeonin.BcryptBase64.encode
      |> to_string

      encrypted_password = hashpwsalt("johnexample")

      user = %User{
        login: Faker.Internet.user_name,
        email: Faker.Internet.email,
        password: "johnexample",
        password_confirmation: "johnexample",
        crypted_password: encrypted_password,
        token: token
      }

      user = user |> Repo.insert!

      %Account{
        balance: 99999.0,
        user_id: user.id,
        token: token} |> Repo.insert!
    end
  end
end
