defmodule Payments.Plug.Authenticate do
  import Plug.Conn
  import Payments.Router.Helpers
  import Phoenix.Controller

  import Ecto.Query

  alias Payments.Repo

  def init(default), do: default

  def call(conn, _default) do
    token = Plug.Conn.get_req_header(conn, "authorization")

    authenticate(conn, token)
  end

  defp authenticate(conn, ["Bearer " <> token]) do
    user = ConCache.get_or_store(:cache, token, fn() ->
      Repo.one(from u in Payments.User,
      where: u.token == ^token)
    end)

    unless user do
      unauthorized(conn)
    else
      assign(conn, :current_user, user)
    end
  end

  defp authenticate(conn, _token) do
    unauthorized(conn)
  end

  defp unauthorized(conn) do
    conn
    |> put_status(:unprocessable_entity)
    |> render(Payments.ChangesetErrorView, "error.json", changeset: %{"message"=> "you are not allowed!"})
    |> halt
  end
end
