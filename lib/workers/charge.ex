defmodule Payments.Worker.Charge do
  import Ecto.Query

  alias Payments.Account
  alias Payments.Repo

  def perform(charge) do
    token = charge["token"]

    account = Repo.one!(
    from u in Account,
    where: u.token == ^token)

    Repo.update!(%{account | balance: account.balance + charge["amount"] })
  end
end
