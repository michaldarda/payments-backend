defmodule Payments.Repo.Migrations.CreateAccount do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :balance, :float
      add :user_id, :integer

      timestamps
    end

  end
end
