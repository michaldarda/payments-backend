defmodule Payments.Repo.Migrations.CreateAccountAction do
  use Ecto.Migration

  def change do
    create table(:account_actions) do
      add :account_id, :integer
      add :description, :string

      timestamps
    end

  end
end
