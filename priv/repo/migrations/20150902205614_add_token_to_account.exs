defmodule Payments.Repo.Migrations.AddTokenToAccount do
  use Ecto.Migration

  def change do
    alter table(:accounts) do
      add :token, :string
    end
  end
end
