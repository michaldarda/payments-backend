#!/bin/bash

docker rm -f payments_db
docker rm -f payments_backend
docker rm -f payments_openresty
docker rm -f authentication_service
docker rm -f authentication_service2
docker rm -f authentication_service3
docker rm -f operation_service
docker rm -f operation_service2
docker rm -f account_service

docker pull michaldarda/payments-backend:latest
docker pull michaldarda/openresty:latest
docker pull michaldarda/payments-authentication-service:latest
docker pull michaldarda/payments-account-service:latest
docker pull michaldarda/payments-operation-service:latest

docker run  --name payments_db \
  -e POSTGRES_PASSWORD=postgres \
  -e PGDATA=/var/lib/postgresql/pgdata \
  -v /home/vagrant/postgres:/var/lib/postgresql \
  -d postgres

docker run --name authentication_service \
  --link payments_db:payments_db \
  -d michaldarda/payments-authentication-service

docker run --name authentication_service2 \
  --link payments_db:payments_db \
  -d michaldarda/payments-authentication-service

docker run --name authentication_service3 \
  --link payments_db:payments_db \
  -d michaldarda/payments-authentication-service

docker run --name payments_backend \
  --link payments_db:payments_db \
  -d michaldarda/payments-backend

docker run --name account_service \
  --link payments_db:payments_db \
  --link authentication_service:auth_service \
  -d michaldarda/payments-account-service

docker run --name operation_service \
  --link authentication_service2:auth_service \
  --link payments_db:payments_db \
  -d michaldarda/payments-operation-service

docker run --name operation_service2 \
  --link authentication_service3:auth_service \
  --link payments_db:payments_db \
  -d michaldarda/payments-operation-service

docker run --name payments_openresty \
  -p 80:80 \
  -p 81:81 \
  -p 82:82 \
  -p 83:83 \
  --link payments_backend:payments_backend  \
  --link authentication_service:authentication_service \
  --link account_service:account_service \
  --link operation_service:operation_service \
  --link operation_service2:operation_service2 \
  -e PGDATA=/var/lib/postgresql/data/pgdata \
  -v /home/vagrant/nginx.conf:/usr/local/openresty/nginx/conf/nginx.conf:ro \
  -v /home/vagrant/cors.conf:/usr/local/openresty/nginx/conf/cors.conf:ro \
  -v /home/vagrant/upstreams.conf:/usr/local/openresty/nginx/conf/upstreams.conf \
  -d michaldarda/openresty \
  sh -c '/usr/local/bin/ep -v /usr/local/openresty/nginx/conf/upstreams.conf -- /usr/local/openresty/nginx/sbin/nginx'

docker run -it payments_backend sh -c 'mix ecto.create'
docker run -it payments_backend sh -c 'mix ecto.migrate'
