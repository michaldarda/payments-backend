defmodule Payments.ControllerTest do
  use Payments.ConnCase

  alias Payments.User
  alias Payments.Account
  alias Payments.AccountAction

  import Ecto.Query

  setup do
    ConCache.delete(:cache, "mytoken")
  end

  test "POST /deposit - unauthorized" do
    conn = conn()
    |> put_req_header("authorization", "Bearer dkdjd")
    |> put_req_header("accept", "application/json")
    |> post "/deposit"

    assert json_response(conn, 422)
  end

  test "POST /deposit" do
    user = %User{
      email: "john@example.com",
      login: "johnexample",
      password: "johnexample",
      password_confirmation: "johnexample",
      crypted_password: "johnexamplef",
      token: "mytoken"}

    user = user |> Repo.insert!

    %Account{
      balance: 10.0,
      user_id: user.id} |> Repo.insert!

    conn = conn()
    |> put_req_header("authorization", "Bearer #{user.token}")
    |> put_req_header("accept", "application/json")
    |> post "/deposit", %{ "transfer" => %{ "amount" => 10, email: user.email }}

    assert json_response(conn, 200)
  end
end
