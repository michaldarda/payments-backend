defmodule Payments.AccountActionTest do
  use Payments.ModelCase

  alias Payments.AccountAction

  @valid_attrs %{account_id: 42, description: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = AccountAction.changeset(%AccountAction{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = AccountAction.changeset(%AccountAction{}, @invalid_attrs)
    refute changeset.valid?
  end
end
