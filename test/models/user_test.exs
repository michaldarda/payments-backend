defmodule Payments.UserTest do
  use Payments.ModelCase

  alias Payments.User

  @valid_attrs %{
    password: "some content",
    password_confirmation: "some content",
    crypted_password: "dddd",
    email: "some content",
    login: "some content",
    token: "some random token"
  }

  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)

    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end
end
