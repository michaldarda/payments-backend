defmodule Payments.Worker.TransferTest do
  alias Payments.Account
  alias Payments.User

  use Payments.ModelCase

  test "it works" do
    user = %User{
      email: "john@example.com",
      login: "johnexample",
      password: "johnexample",
      password_confirmation: "johnexample",
      crypted_password: "johnexamplef",
      token: "mytoken"}

    user = user |> Repo.insert!

    account = %Account{
      balance: 10.0,
      user_id: user.id} |> Repo.insert!

    user1 = %User{
      email: "john1@example.com",
      login: "johnexample1",
      password: "johnexample",
      password_confirmation: "johnexample",
      crypted_password: "johnexamplef",
      token: "mytoken"}

    user1 = user1 |> Repo.insert!

    account1 = %Account{
      balance: 10.0,
      user_id: user1.id} |> Repo.insert!

    Payments.Worker.Transfer.perform(user.id, %{ "amount" => 5, "email" => user1.email })

    account = Repo.one!(from a in Account, where: a.id == ^account.id)
    account1 = Repo.one!(from a in Account, where: a.id == ^account1.id)

    assert account.balance == 5.0
    assert account1.balance == 15.0
  end

  test "it doesnt lead to negative balance" do
    user = %User{
      email: "john@example.com",
      login: "johnexample",
      password: "johnexample",
      password_confirmation: "johnexample",
      crypted_password: "johnexamplef",
      token: "mytoken"}

    user = user |> Repo.insert!

    account = %Account{
      balance: 10.0,
      user_id: user.id} |> Repo.insert!

    user1 = %User{
      email: "john1@example.com",
      login: "johnexample1",
      password: "johnexample",
      password_confirmation: "johnexample",
      crypted_password: "johnexamplef",
      token: "mytoken"}

    user1 = user1 |> Repo.insert!

    account1 = %Account{
      balance: 10.0,
      user_id: user1.id} |> Repo.insert!

    Payments.Worker.Transfer.perform(user.id, %{ "amount" => 50, "email" => user1.email })

    account = Repo.one!(from a in Account, where: a.id == ^account.id)
    account1 = Repo.one!(from a in Account, where: a.id == ^account1.id)

    assert account.balance == 10.0
    assert account1.balance == 10.0
  end
end
