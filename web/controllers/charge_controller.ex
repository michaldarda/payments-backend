defmodule Payments.ChargeController do
  use Payments.Web, :controller

  @accepted_fields ~w(GENEROUS-BANK-1234567 MEAN-BANK-1234567)

  plug :action

  def create(conn, %{ "charge" => charge }) do
    cert = Plug.Conn.get_req_header(conn, "cert") |> hd

    unless Enum.member?(@accepted_fields, cert) do
      conn
      |> put_status(:unauthorized)
      |> render Payments.ChangesetErrorView, "error.json", changeset: %{ security: "Possible break-in attempt!. Incident will be logged." }
    else
      Task.async fn() ->
        Payments.Worker.Charge.perform(charge)
      end

      render conn, account_balance: %{ balance: 0.0 }
    end
  end
end
