defmodule Payments.HaCheckController do
  use Payments.Web, :controller

  plug :action

  def show(conn, _params) do
    conn |> text "OK"
  end
end
