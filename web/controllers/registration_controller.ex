defmodule Payments.RegistrationController do
  use Payments.Web, :controller
  alias Payments.Registration

  plug :action

  def create(conn, %{"user" => user_params}) do
    # This usecase should take blocks, success, failure
    changeset = Registration.proceed(user_params)

    if changeset.valid? do
      user = Repo.insert!(changeset)

      # TODO: move to separate usecase CreateAccount
      Payments.Account.changeset(
        %Payments.Account{},
        %{user_id: user.id, balance: 0.0})
        |>  Repo.insert!

      render conn, user: user
    else
      conn
      |> put_status(:unprocessable_entity)
      |> render(Payments.ChangesetErrorView, "error.json", changeset: changeset)
    end
  end
end
