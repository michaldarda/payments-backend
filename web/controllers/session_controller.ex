defmodule Payments.SessionController do
  use Payments.Web, :controller

  import Ecto.Query

  plug :action

  def create(conn, %{"user" => user_params}) do
    query = from u in Payments.User,
    where: u.login == ^user_params["login"]

    user = Repo.one(query)

    pass_match = Comeonin.Bcrypt.checkpw(user_params["password"], user.crypted_password)

    if pass_match do
      render conn, session: user
    else
      conn
      |> put_status(:unprocessable_entity)
      |> render(Payments.ChangesetErrorView, "error.json", changeset: %{"error" => "password or login is incorrect"})
    end
  end
end
