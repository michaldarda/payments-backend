defmodule Payments.TransferController do
  use Payments.Web, :controller

  plug Payments.Plug.Authenticate
  plug :action

  def create(conn, %{ "transfer" => transfer }) do
    Task.async fn() ->
      Payments.Worker.Transfer.perform(current_user(conn).id, transfer)
    end

    render conn, account_balance: %{ balance: 0.0 }
  end

  defp current_user(conn) do
    conn.assigns.current_user
  end
end
