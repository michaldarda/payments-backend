defmodule Payments.User do
  use Payments.Web, :model

  schema "users" do
    field :login, :string
    field :email, :string
    field :crypted_password, :string
    field :token, :string

    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    has_one :account, Payments.Account

    timestamps
  end

  @required_fields ~w(login email password password_confirmation token crypted_password)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If `params` are nil, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> validate_unique(:email, on: Payments.Repo, downcase: true)
    |> validate_unique(:login, on: Payments.Repo, downcase: true)
    |> validate_length(:password, min: 6)
    |> validate_confirmation(:password)
  end
end
