defmodule Payments.Router do
  use Payments.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Payments do
    pipe_through :api

    resource "/ha_check", HaCheckController, only: [:show]

    post "/sessions", SessionController, :create
    post "/registration", RegistrationController, :create

    post "/deposit", TransferController, :create

    get "/account_balance", AccountBalanceController, :show

    resources "/account_actions", AccountActionController, only: [:index]

    resources "/charges", ChargeController, only: [:create]
  end
end
