defmodule Payments.AccountView do
  use Payments.Web, :view

  def render("show.json", %{account: account}) do
    %{data: render_one(account, "account.json")}
  end

  def render("account.json", %{account: account}) do
    %{balance: account.balance}
  end
end
