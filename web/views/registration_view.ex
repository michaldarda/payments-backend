defmodule Payments.RegistrationView do
  use Payments.Web, :view

  def render("create.json", %{user: user}) do
    %{
      "email" => user.email,
      "token" => user.token
    }
  end
end
