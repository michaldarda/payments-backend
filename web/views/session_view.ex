defmodule Payments.SessionView do
  use Payments.Web, :view

  def render("create.json", %{session: session}) do
    %{
      "email" => session.email,
      "token" => session.token
    }
  end
end
